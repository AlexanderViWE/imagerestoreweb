from logging import getLogger
from urllib.request import urlopen

from PIL import Image
from celery import states
from celery.result import AsyncResult
from flask import (Blueprint, render_template, request, flash, redirect,
                   send_file, url_for)

import tasks
from .celery_app import celery_app
from .forms import ProcessImageForm, ImageInpaintingForm
from .image_storage_client import save_file2storage
from .utils import image2bytes

logger = getLogger(__name__)

bp = Blueprint('image_editor', __name__, url_prefix=None)


def _image_colorize(file, filename: str) -> str:
    file_url = save_file2storage(file, filename)
    task = tasks.colorize_image.delay(file_url)
    return task.id


def _image_upscale(file, filename: str) -> str:
    file_url = save_file2storage(file, filename)
    task = tasks.upscale_image.delay(file_url)
    return task.id


def _image_inpainting(image, image_name, mask, mask_name):
    image_url = save_file2storage(image, image_name)
    mask_url = save_file2storage(mask, mask_name)
    task_id = tasks.image_inpainting_task.delay(image_url, mask_url)
    return task_id.id


def _try_get_results(task_id: str):
    # Check image processing result...
    task = AsyncResult(task_id, app=celery_app)

    if task.status != states.SUCCESS:
        return None

    try:
        result = task.get(5)
    except TimeoutError:
        return None

    logger.info("Receive %s", result)

    try:
        return Image.open(urlopen(result))
    except FileNotFoundError as err:
        logger.warning(err)
        return None


@bp.get('/')
def index():
    return render_template('index.html')


@bp.route('/image_colorize', methods=['GET', 'POST'])
def image_colorize():
    if request.method == 'POST':

        form = ProcessImageForm(request)
        if not form.validate():
            for err in form.errors:
                flash(err)
            return redirect(request.url)

        task_id = _image_colorize(form.clean_data['file'],
                                  form.clean_data['file'].filename)

        return redirect(url_for('image_editor.check_task_result', task_id=task_id))

    return render_template('image_form.html')


@bp.route('/image_upscale', methods=['GET', 'POST'])
def image_upscale():
    if request.method == 'POST':

        form = ProcessImageForm(request)
        if not form.validate():
            for err in form.errors:
                flash(err)
            return redirect(request.url)

        task_id = _image_upscale(form.clean_data['file'],
                                 form.clean_data['file'].filename)

        return redirect(url_for('image_editor.check_task_result', task_id=task_id))

    return render_template('image_form.html')


@bp.route('/image_inpainting', methods=['GET', 'POST'])
def image_inpainting():
    if request.method == 'POST':

        form = ImageInpaintingForm(request)
        if not form.validate():
            for err in form.errors:
                flash(err)
            return redirect(request.url)

        task_id = _image_inpainting(form.clean_data['image'], form.clean_data['image'].filename,
                                    form.clean_data['mask'], form.clean_data['mask'].filename)

        return url_for('image_editor.check_task_result', task_id=task_id)

    return render_template('inpaint_form.html')


@bp.get('/tasks/<task_id>/')
def check_task_result(task_id: str):
    task = AsyncResult(task_id, app=celery_app)
    is_complete = task.status != states.SUCCESS
    return render_template('wait.html', is_complete=is_complete, task_id=task_id)


@bp.get('/tasks/<task_id>/download')
def task_result(task_id: str):
    image = _try_get_results(task_id)

    if image is None:
        return render_template('wait.html', task_id=task_id)

    # TODO: mimetype, _ = mimetypes.guess_type(str(filepath))
    mimetype = 'image/png'
    return send_file(image2bytes(image), mimetype=mimetype)
