from celery import Celery
import os

from logging import getLogger

logger = getLogger(__name__)


def create_app():
    broker = os.environ.get("CELERY_BROKER")
    backend = os.environ.get("CELERY_BACKEND")
    # if __debug__:
    #     backend = broker = 'redis://192.168.99.100:6379/0'
    app = Celery(__name__, backend=backend, broker=broker, include=['web_app.tasks'])
    return app


celery_app = create_app()
if __name__ == '__main__':
    celery_app.start()
