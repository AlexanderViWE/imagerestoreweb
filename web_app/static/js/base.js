// $.get({
// 	url: "/tasks/",
// 	type: "get",
// 	dataType: "html",
// 	success: function function_name(argument) {
// 		// body...
// 	},
// 	fail: function function_name(argument) {
// 		// body...
// 	}
// });


function setPreviewElement(fileInput, preview) {
    preview.style.display = 'none';
    fileInput.addEventListener('change', (e) => {
        const [file] = fileInput.files;
        if (file) {
            preview.src = URL.createObjectURL(file);
            preview.style.display = 'block';
        }
    })
}
