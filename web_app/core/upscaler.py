from pathlib import Path

import torch
from PIL import Image
from RealESRGAN import RealESRGAN


class Upscaler:
    """
    https://github.com/ai-forever/Real-ESRGAN?tab=readme-ov-file
    """

    def __init__(self):
        device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')
        self.model = RealESRGAN(device, scale=2)

        modelpath = Path(__file__).parent.parent.parent / 'models' / 'RealESRGAN_x2.pth'
        self.model.load_weights(modelpath, download=False)

    def __call__(self, image: Image.Image) -> Image.Image:
        image = image.convert('RGB')
        return self.model.predict(image)
