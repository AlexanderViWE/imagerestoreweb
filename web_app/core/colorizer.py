from pathlib import Path

import numpy
import torch
from PIL import Image

from web_app.core.colorizers import siggraph17, preprocess_img, postprocess_tens


class Colorizer:
    """
    https://github.com/richzhang/colorization/tree/master
    """

    def __init__(self):
        self.model = siggraph17(pretrained=False).eval()

        modelpath = Path(__file__).parent.parent.parent / 'models' / 'siggraph17-df00044c.pth'
        self.model.load_state_dict(torch.load(str(modelpath), map_location='cpu'))

    def __call__(self, image: Image.Image) -> Image.Image:
        image = numpy.asarray(image)
        if image.ndim == 2:
            image = numpy.tile(image[:, :, None], 3)
        (tens_l_orig, tens_l_rs) = preprocess_img(image, HW=(256, 256))
        out_img = postprocess_tens(tens_l_orig, self.model(tens_l_rs).cpu())
        out_img = numpy.round(out_img * 255).astype(numpy.uint8)
        return Image.fromarray(out_img)
