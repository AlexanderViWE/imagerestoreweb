import random

from flask import Flask

from .config import *


def create_app():
    """Create and configure an instance of the Flask application."""

    app = Flask(__name__, instance_relative_config=True)

    try:
        os.mkdir(app.instance_path)
    except FileExistsError:
        pass

    try:
        os.makedirs(UPLOAD_FOLDER)
    except FileExistsError:
        pass

    app.config.from_mapping(
        SECRET_KEY='%032x' % random.getrandbits(256),
        UPLOAD_FOLDER=UPLOAD_FOLDER,
        IMAGE_STORAGE=IMAGE_STORAGE,
        ALLOWED_EXTENSIONS=ALLOWED_EXTENSIONS,
    )

    from . import image_editor
    app.register_blueprint(image_editor.bp)

    from . import image_storage
    app.register_blueprint(image_storage.bp)

    return app
