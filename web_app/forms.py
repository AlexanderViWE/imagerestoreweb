from flask import current_app
from werkzeug.utils import secure_filename


def allowed_file(filename):
    return ('.' in filename
            and filename.rsplit('.', 1)[1].lower() in current_app.config['ALLOWED_EXTENSIONS'])


class Form:
    """
    Базовый класс для обработки HTML форм.
    """
    def __init__(self, request):
        self.request = request
        self.errors = []
        self.clean_data = {}

    def validate(self):
        return False


class ProcessImageForm(Form):
    def validate(self):
        file = self.request.files.get('images', None)
        if not file:
            self.errors.append('No file part')
        else:
            file.filename = filename = secure_filename(file.filename)

            if filename == '' or not allowed_file(filename):
                self.errors.append('Invalid file format')

        self.clean_data['file'] = file

        return len(self.errors) == 0


class ImageInpaintingForm(Form):
    def validate(self):

        file = self.request.files.get('image', None)
        if not file:
            self.errors.append('No file part')
        else:
            file.filename = filename = secure_filename(file.filename)
            if filename == '' or not allowed_file(filename):
                self.errors.append('Invalid file format')
        self.clean_data['image'] = file

        mask = self.request.files.get('mask', None)
        if not mask:
            self.errors.append('No mask part')
        else:
            mask.filename = filename = secure_filename(mask.filename)
            if filename == '' or not allowed_file(filename):
                self.errors.append('Invalid file format')
        self.clean_data['mask'] = mask

        return len(self.errors) == 0
