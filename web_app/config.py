import os

# Расширения файлов, разрешенных для отправки пользователем
ALLOWED_EXTENSIONS = {'png', 'jpg', 'jpeg', 'jfif'}

# URL для получения файлов
IMAGE_STORAGE = os.environ.get("IMAGE_STORAGE")

# Путь до директории для хранения фалов
UPLOAD_FOLDER = os.environ.get("DATA_STORAGE", "data")
