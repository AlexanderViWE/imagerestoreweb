import io
from typing import Union
import numpy
from PIL import Image
from werkzeug.datastructures import FileStorage


def image2bytes(image: Union[numpy.ndarray, Image.Image], format='PNG'):
    file_object = io.BytesIO()
    if isinstance(image, numpy.ndarray):
        image = Image.fromarray(image)
    elif isinstance(image, FileStorage):
        image = Image.open(image)
    image.save(file_object, format)
    file_object.seek(0)
    return file_object
