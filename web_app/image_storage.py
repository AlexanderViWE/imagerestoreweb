import os.path
from logging import getLogger

import numpy
from PIL import Image
from flask import (Blueprint, request, current_app, send_file, abort, Response)
from werkzeug.utils import secure_filename

from .utils import image2bytes

logger = getLogger(__name__)

bp = Blueprint('image_storage', __name__, url_prefix=None)


@bp.get('/files/<path:filepath>')
def get_image(filepath: str):
    save_filepath = os.path.join(current_app.config['UPLOAD_FOLDER'], filepath)
    try:
        image = Image.open(save_filepath)
    except FileNotFoundError:
        abort(404)
    image = image2bytes(image)
    return send_file(image, mimetype='image/gif')


@bp.post('/files/')
def store_image():
    file = request.files['images']
    filename = secure_filename(file.filename)

    if file:
        # Save file to storage
        file.save(os.path.join(current_app.config['UPLOAD_FOLDER'], filename))
        return Response("", status=201)
    abort(404)


@bp.get('/files/')
def get_images():
    files = os.listdir(str(os.path.join(current_app.config['UPLOAD_FOLDER'])))
    return '; '.join(files)
