"""
Запуск асинхронной обработки изображений.

TODO: Переписать. Каждая модель должна запускаться на отдельном сервере со
 своим API, а тут должен быть просто вызов этих API.
"""

import hashlib
import os
from logging import getLogger
from urllib.request import urlopen

from PIL import Image
from celery import Task
from simple_lama_inpainting import SimpleLama

from web_app.celery_app import celery_app
from web_app.core.colorizer import Colorizer
from web_app.core.upscaler import Upscaler
from web_app.image_storage_client import save_file2storage

logger = getLogger(__name__)


def _load_image(file_url) -> Image.Image:
    return Image.open(urlopen(file_url))


def generate_output_filename(output: Image.Image, base_filename: str, extension='PNG'):
    filename = os.path.basename(base_filename)
    extension = filename.split('.')[-1] if extension is None else extension
    hashcode = hashlib.md5(bytes(output.tobytes())).hexdigest()

    filename = f"{hashcode}.{extension}"
    logger.info("Generate '%s' for '%s'", filename, base_filename)
    return filename


class ModelLoaderTask(Task):
    """
    Abstraction of Celery's Task class to support loading ML model.
    """

    model_class = None

    def __init__(self):
        super().__init__()
        self.model = None

    def __call__(self, *args, **kwargs):
        if not self.model:
            logger.info("Loading Model %s...", self.model_class.__name__)
            self.model = self.model_class()
            logger.info("Model loaded %s.", self.model_class.__name__)
        return self.run(*args, **kwargs)


class PredictTask(ModelLoaderTask):
    def _run(self, filepath):
        # Load image
        image = _load_image(filepath)

        # Inference
        logger.info("Start model")
        result = self.model(image)
        logger.info("End model")

        # Create a filename for the result
        result_filename = generate_output_filename(result, filepath)
        result_file_url = save_file2storage(result, result_filename)
        return result_file_url


class ColorizeTask(PredictTask):
    model_class = Colorizer


@celery_app.task(bind=True,
                 base=ColorizeTask)
def colorize_image(self, filepath):
    return self._run(filepath)


class UpscaleTask(PredictTask):
    model_class = Upscaler


@celery_app.task(bind=True,
                 base=UpscaleTask)
def upscale_image(self, filepath):
    return self._run(filepath)


class InpaintingTask(ModelLoaderTask):
    model_class = SimpleLama

    def _run(self, imagepath, maskapth):
        # Load image
        image = _load_image(imagepath).convert("RGB")
        mask = _load_image(maskapth).convert('L')

        self.model(image, mask)
        # Inference
        logger.info("Start model")
        result = self.model(image, mask)
        logger.info("End model")

        # Create a filename for the result
        result_filename = generate_output_filename(result, imagepath)
        result_file_url = save_file2storage(result, result_filename)
        return result_file_url


@celery_app.task(bind=True,
                 base=InpaintingTask)
def image_inpainting_task(self, imagepath, maskpath):
    return self._run(imagepath, maskpath)
