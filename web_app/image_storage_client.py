import logging
import os.path
from posixpath import join as urljoin  # from urllib.parse import urljoin

import requests

from .config import IMAGE_STORAGE
from .utils import image2bytes

logger = logging.getLogger(__name__)


def save_file2storage(file, filename):
    files = [
        ('images', (str(filename), image2bytes(file), 'image/png'))
    ]
    response = requests.post(get_storage_url(), files=files)
    logger.debug("File storage response: %s", response)

    return urljoin(IMAGE_STORAGE, 'files', filename)


def get_storage_url() -> str:
    return urljoin(IMAGE_STORAGE, 'files')


def get_url_for(path):
    return os.path.join(IMAGE_STORAGE, path)
