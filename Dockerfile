FROM python:3.10-slim-bullseye
LABEL author=Sasha

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

RUN apt-get update
# RUN apt-get upgrade -y

# RUN addgroup -S user \
#     && adduser -S -G user -s /bin/sh -D user
WORKDIR /home/user
RUN pip3 install --progress-bar off --upgrade pip


RUN pip3 install torch torchvision --index-url https://download.pytorch.org/whl/cpu
RUN apt-get install git ffmpeg libsm6 libxext6 -y
RUN pip3 install git+https://github.com/sberbank-ai/Real-ESRGAN.git


# Specific installation
COPY requirements.txt .
RUN pip3 install --progress-bar off -r requirements.txt
RUN pip3 install requests

COPY . .

EXPOSE 5000
