import os.path
import time
from logging import getLogger

import click
import torch
from torch.utils.data import Dataset
from torchvision import transforms
from torch.utils.tensorboard import SummaryWriter
from ml.dataset import ColorDataset
from ml.models.unet import UNet, Descriptor
from ml.models.eff_unet import EffB0Unet

from tqdm import tqdm

logger = getLogger(__name__)

device = "cuda" if torch.cuda.is_available() \
    else "mps" if torch.backends.mps.is_available() \
    else "cpu"

logger.info("Torch device is '%s'", device)

# writer: SummaryWriter | None = None

DATA_PATH = r"D:\Projects\res\gray2color"


# def validate(model, loss_function, validation_loader):
#     model.eval()
#     total_vloss = 0.
#
#     # Disable gradient computation and reduce memory consumption.
#     with torch.no_grad():
#         for images, targets in validation_loader:
#             voutputs = model(images)
#             vloss = loss_function(voutputs, targets)
#             total_vloss += vloss
#
#     avg_vloss = total_vloss / len(validation_loader.dataset)
#     return avg_vloss
#
#
# def train(model, epochs, train_loader, validation_loader=None, ):
#     loss_function = torch.nn.MSELoss()
#
#     optimizer = torch.optim.RMSprop(
#         model.parameters(),
#         lr=1e-4
#     )
#
#     for epoch in range(epochs):
#         logger.info(f"Epoch {epoch}")
#         total_loss = 0.
#
#         # Train
#
#         with alive_bar(len(train_loader)) as bar:
#
#             model.train()
#             for images, targets in train_loader:
#                 reconstructed = model(images)
#                 loss = loss_function(reconstructed, targets)
#
#                 # The gradients are set to zero,
#                 # the gradient is computed and stored.
#                 # .step() performs parameter update
#                 optimizer.zero_grad()
#                 loss.backward()
#                 optimizer.step()
#
#                 total_loss += loss
#
#                 bar.text(f"loss={total_loss / (bar.current + 1)}")
#                 bar()
#
#         avg_loss = total_loss / len(train_loader.dataset)
#         writer.add_scalar('Loss_Training', avg_loss, epoch + 1)
#         logger.info(f'LOSS train {avg_loss}')
#
#         # Validate
#         if validation_loader:
#             avg_vloss = validate(model, loss_function, validation_loader)
#             logger.info(f'LOSS train valid {avg_vloss}')
#             writer.add_scalar('Loss_Validation', avg_vloss, epoch + 1)
#
#         writer.flush()
#
#         torch.save(model, "model.pt")
#
#         # # Track best performance, and save the model's state
#         # if avg_vloss < best_vloss:
#         #     best_vloss = avg_vloss
#         #     model_path = 'model_{}_{}'.format(timestamp, epoch_number)
#         #     torch.save(model.state_dict(), model_path)


def merge_batch(l_components, ab_components):
    lab_tensor = torch.concat([l_components, ab_components], dim=1)
    return lab_tensor


def train_gun(generator, discriminator, epochs, train_loader):
    # Loss functions
    loss_discriminator = torch.nn.BCELoss()
    loss_reconstruction = torch.nn.L1Loss()

    batch_size = train_loader.batch_size

    gen_optimizer = torch.optim.Adam(
        generator.parameters(),
        lr=1e-4
    )
    discr_optimizer = torch.optim.RMSprop(
        discriminator.parameters(),
        lr=1e-4
    )

    for epoch in range(epochs):
        total_g_loss = 0.
        total_d_loss = 0.

        # Train

        # with alive_bar(len(train_loader), force_tty=True, title=f'Epoch {epoch + 1}/{epochs}') as bar:

        with tqdm(total=len(train_loader), desc=f'Epoch {epoch}/{epochs}') as bar:
            for images, targets in train_loader:
                images = images.to(device)
                targets = targets.to(device)

                valid = torch.ones(images.size()[0], 1, requires_grad=False).to(device)
                fake = torch.zeros(images.size()[0], 1, requires_grad=False).to(device)

                # -----------------
                #  Train Generator
                # -----------------

                gen_optimizer.zero_grad()

                reconstructed = generator(images)
                validity = discriminator(merge_batch(images, reconstructed.detach()))

                # Loss ошибкки дискриминатора + Loss восстановления изображения
                g_loss = loss_discriminator(validity, valid)
                g_loss = g_loss + loss_reconstruction(reconstructed, targets)

                g_loss.backward()
                gen_optimizer.step()

                # ---------------------
                #  Train Discriminator
                # ---------------------

                discr_optimizer.zero_grad()

                validity_real = discriminator(merge_batch(images, targets))
                discr_real_loss = loss_discriminator(validity_real, valid)

                validity_fake = discriminator(merge_batch(images, reconstructed.detach()))
                discr_fake_loss = loss_discriminator(validity_fake, fake)

                d_loss = (discr_real_loss + discr_fake_loss) / 2

                d_loss.backward()
                discr_optimizer.step()

                total_g_loss += g_loss.item()
                total_d_loss += d_loss.item()

                bar.set_postfix_str(f"D Loss: {d_loss.item()}, G Loss: {g_loss.item()}")
                bar.update()

            avg_g_loss = total_g_loss / len(train_loader)
            avg_d_loss = total_d_loss / len(train_loader)
            writer.add_scalars('Loss', {'Generator': avg_g_loss, 'Discriminator': avg_d_loss},
                               epoch + 1)
            writer.flush()
            logger.info(f"[G loss: {avg_g_loss}] [D loss: {avg_d_loss}]")

        torch.save(generator, "generator.pt")
        torch.save(discriminator, "discriminator.pt")


@click.command
@click.option('-e', '--epochs', default=1, type=int)
@click.option('-b', '--batch_size', default=4, type=int)
@click.option('--name', default='', type=str)
def main(epochs=1, batch_size=4, name=None):
    """

    1) Прочитать изображение цветное и серое (g).
    2) Цветное перевести в Lab и разбить на составляющие l, a, b
    3) Тренировать модель F(g)=~a, ~b

    """
    global writer

    if name is None:
        log_dir = None
    else:
        current_time = time.strftime("%b%d_%H-%M-%S")
        log_dir = os.path.join("runs", f"{current_time}_{name}")
    writer = SummaryWriter(log_dir=log_dir)

    image_transform = transforms.Compose([
        transforms.ToTensor(),
        lambda x: x / torch.tensor([100, 128, 128]).unsqueeze(1, ).unsqueeze(1, ),
        transforms.RandomResizedCrop(size=(224, 224), antialias=True),
        transforms.RandomHorizontalFlip(),
        transforms.ConvertImageDtype(torch.float)
    ])

    dataset = ColorDataset(
        DATA_PATH,
        limit=20,
        transform=image_transform
    )
    train_loader = torch.utils.data.DataLoader(dataset=dataset,
                                               batch_size=batch_size,
                                               shuffle=True)

    model = torch.nn.Sequential(
        EffB0Unet(1, 2),
        torch.nn.Tanh()
    ).to(device)

    discr = Descriptor(3).to(device)

    # train(model, epochs, train_loader, validation_loader=train_loader)
    train_gun(model, discr, epochs, train_loader)


if __name__ == '__main__':
    # main()

    # import hiddenlayer as hl
    from torchview import draw_graph

    model = torch.nn.Sequential(
        EffB0Unet(1, 2),
        torch.nn.Tanh()
    ).to(device)

    model_graph = draw_graph(model, input_size=(1, 1, 224, 224), expand_nested=True)
    print(model_graph.visual_graph.render(format='png'))


    #
    # transforms = [hl.transforms.Prune('Constant')]  # Removes Constant nodes from graph.
    #
    # graph = hl.build_graph(model, batch.text, transforms=transforms)
    # graph.theme = hl.graph.THEMES['blue'].copy()
    # graph.save('rnn_hiddenlayer', format='png')
