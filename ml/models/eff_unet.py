import torch
import torch.nn as nn
import torch.nn.functional as F
import torchvision.models as models

from ml.models.unet import DoubleConv


class Up(nn.Module):
    """Upscaling then double conv"""

    def __init__(self, in_channels, in2_channels, out_channels):
        super().__init__()
        self.up = nn.ConvTranspose2d(in_channels, in_channels // 2, kernel_size=2, stride=2)
        self.conv = DoubleConv(in_channels // 2 + in2_channels, out_channels)

    def forward(self, x1, x2):
        x1 = self.up(x1)
        x = torch.cat([x2, x1], dim=1)
        return self.conv(x)


class EffB0Unet(nn.Module):
    """(convolution => [BN] => LeakyReLU) * 2"""

    def __init__(self, in_channels, n_classes):
        super().__init__()

        self.conv1 = nn.Conv2d(in_channels, 3, 3, padding=1)

        model = models.efficientnet_b0()
        future_extraction = list(model.children())[0][:-1]
        # Convert first input Conv2D size from 3 to 1
        # future_extraction[0][0] = nn.Parameter(future_extraction[0][0].weight.sum(dim=1).unsqueeze(1))

        self.blocks = list(future_extraction)

        self.up1 = Up(320, 112, 112)
        self.up2 = Up(112, 40, 40)
        self.up3 = Up(40, 24, 24)
        self.up4 = Up(24, 16, 16)
        self.up5 = nn.Upsample(scale_factor=2)

        self.outc = nn.Conv2d(16, n_classes, kernel_size=1)

    def forward(self, x_input):
        x = x_input
        x = self.conv1(x)

        rets = []
        for block in self.blocks:
            x = block(x)
            rets.append(x)

        r1, r2, r3, r4, r5, r6, r7, r8 = rets

        x = self.up1(r8, r6)  # 7x7 and 14x14
        x = self.up2(x, r4)  # 14x14 and 28x28
        x = self.up3(x, r3)  # 28x28 and 56x56
        x = self.up4(x, r2)  # 56x56 and 112x112
        x = self.up5(x)  # 224x224
        return self.outc(x)


if __name__ == '__main__':
    model = EffB0Unet(1, 2)
    print(model)

    from torchsummary import summary

    summary(model, (1, 224, 224), 3)
