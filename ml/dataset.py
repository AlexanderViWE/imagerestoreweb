import os.path as path
from glob import glob
from typing import Tuple, Callable

import torch
import torchvision
from skimage import color, io
from torch.utils.data import Dataset


class ColorDataset(Dataset):
    """
    Класс для упращенного доступа к данным.

    __getitem__() -> (Серое изображение, Цветное изображение)
    """

    def __init__(self, datapath, limit=None, transform: Callable = None):
        dataframe = glob(path.join(datapath, 'train_color', '*'))
        if limit is not None:
            dataframe = dataframe[:limit]

        self.dataframe = dataframe
        self.transform = transform or torchvision.transforms.ToTensor()

    def __len__(self):
        return len(self.dataframe)

    def __getitem__(self, idx) -> Tuple:
        data = self.dataframe[idx]

        # image = cv2.imread(data, cv2.IMREAD_COLOR)
        # lab_image = cv2.cvtColor(image, cv2.COLOR_BGR2Lab)

        image = io.imread(data)
        lab_image = color.rgb2lab(image)

        tensor = self.transform(lab_image)

        gray, colored = torch.split(tensor, [1, 2])

        return gray, colored
