import click
import torch
from ml.models.unet import UNet
import matplotlib.pylab as plt


@click.command()
@click.option('-m', '--model', 'model_path', required=True, default=1,
              help='Path to model.', )
def main(model_path=None):
    model = UNet(1, 3)
    torch.load_state_dict(torch.load(model_path))
    model.eval()

    predict = model()

    plt.imshow()
    # ...


if __name__ == '__main__':
    main()
